name := "betex-engine"

organization := "dk.betex"

version := "0.3-SNAPSHOT"

scalaVersion := "2.10.1"

libraryDependencies ++= Seq(
  "org.apache.commons" % "commons-math" % "2.2",
  "junit" % "junit" % "4.11" % "test",
  "com.novocode" % "junit-interface" % "0.10-M3" % "test"
)

testOptions += Tests.Argument(TestFrameworks.JUnit, "-q", "-v")